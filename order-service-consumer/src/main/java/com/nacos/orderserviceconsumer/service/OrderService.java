package com.nacos.orderserviceconsumer.service;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.nacos.orderserviceconsumer.entity.Order;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-23 17:17  星期一
 * @Description:
 */
public interface OrderService {

    Boolean create(Order order);



    String hello(String limit);
}
