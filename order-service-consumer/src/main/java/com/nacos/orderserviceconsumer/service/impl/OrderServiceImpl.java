package com.nacos.orderserviceconsumer.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.nacos.orderserviceconsumer.dao.OrderMapper;
import com.nacos.orderserviceconsumer.entity.Order;
import com.nacos.orderserviceconsumer.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-23 17:17  星期一
 * @Description:
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public Boolean create(Order order) {
        log.info("创建订单开始");
        int i = 1 / 0;
        int index = orderMapper.insert(order);
        log.info("创建订单结束");
        return index > 0;
    }


    @SentinelResource(value = "limit", defaultFallback = "defaultFallback", blockHandler = "handlerException", blockHandlerClass = {BlockException.class})
    @Override
    public String hello(String limit) {
        return "阈值类型 QPS  流控模式直接";
    }

    /**
     * 自定义简单的 服务限流或者降级回调方法
     *
     * @return
     */
    public String defaultFallback() {
        return "太拥挤了 ~ 请稍后重试 ";
    }

    public String handlerException() {
        return "测试超出流量限制的部分是否会进入到blockHandler的方法。";
    }


}
