package com.nacos.orderserviceconsumer.controller;

import org.springframework.stereotype.Component;

/**
 * @author LIHUA
 */
@Component
public class OrderQueryService {


    public String queryOrderInfo(String orderId) {
        System.out.println("获取订单信息:" + orderId);
        return System.currentTimeMillis() + "--return OrderInfo :" + orderId;
    }


}

