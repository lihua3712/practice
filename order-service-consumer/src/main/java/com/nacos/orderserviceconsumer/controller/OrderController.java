package com.nacos.orderserviceconsumer.controller;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphO;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.nacos.orderserviceconsumer.entity.Order;
import com.nacos.orderserviceconsumer.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;


/**
 * @author LIHUA
 */
@RestController
@RequestMapping("order")
@RefreshScope    //动态刷新配置，重要
public class OrderController {

    private Logger log = LoggerFactory.getLogger(OrderController.class);

    private static final String KEY = "HelloWorld";

    /**
     * spring常规取值
     */
    @Value("${merber.name}")
    private String merberName;

    @Value("${merber.age}")
    private String merberAge;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderQueryService orderQueryService;

    @GetMapping("get")
    @SentinelResource(value = "resource", blockHandler = "blockHandler")
    public String get() {
        log.info("进入请求。。。");
        //通过服务名的方式调用远程服务（非ip端口）
        String result = restTemplate.getForObject("http://course-service/course/list", String.class);
        log.info("结束请求。。。");
        return result + "，merberName：" + merberName + "，merberAge：" + merberAge;
    }

    /**
     * 创建订单
     *
     * @param userId
     * @param productId
     * @return
     */
    @GetMapping("create")
    public Boolean create(long userId, long productId) {
        Order order = new Order();
        order.setUserId(userId);
        order.setProductId(productId);
        order.setCount(1);
        order.setMoney(BigDecimal.valueOf(88));
        order.setStatus(0);
        return orderService.create(order);
    }


    /**
     * 限流实现方式一: 抛出异常的方式定义资源
     * 测试，当QPS > 2时，接口返回：
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/getOrder1")
    public String queryOrder1(@RequestParam("orderId") String orderId) {
        Entry entry = null;
        // 资源名
        String resourceName = KEY;
        try {
            // entry可以理解成入口登记
            entry = SphU.entry(resourceName);
            // 被保护的逻辑, 这里为订单查询接口
            return orderQueryService.queryOrderInfo(orderId);
        } catch (BlockException blockException) {
            // 接口被限流的时候, 会进入到这里
            log.warn("---getOrder接口被限流了---, exception: ", blockException);
            return "接口限流, 返回空";
        } finally {
            // SphU.entry(xxx) 需要与 entry.exit() 成对出现,否则会导致调用链记录异常
            if (entry != null) {
                entry.exit();
            }
        }
    }

    /**
     * 限流实现方式二: 抛出异常的方式定义资源
     * 测试，当QPS > 2时，接口返回：
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/getOrder2")
    public String queryOrder2(@RequestParam("orderId") String orderId) {
        if (SphO.entry("HelloWorld2")) {
            try {
                return System.currentTimeMillis() + "--return OrderInfo :" + orderId;
            } finally {
                SphO.exit();
            }
        } else {
            return "系统繁忙,请稍后！";
        }
    }

    /**
     * 限流实现方式三: 注解方式定义资源
     * 测试，当QPS > 2时，接口返回：
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/getOrder3")
    @SentinelResource(value = "HelloWorld3", blockHandler = "blockHandler")
    public String queryOrder3(@RequestParam("orderId") String orderId) {
        return System.currentTimeMillis() + "--return OrderInfo :" + orderId;
    }

    public String blockHandler(BlockException e) {
        e.printStackTrace();
        return "系统繁忙，请稍后再试!!";
    }

    /**
     * 定义限流规则
     *
     * @PostConstruct构造方法执行完后执行方法，定义和加载限流规则
     */
    /*@PostConstruct
    public void initFlowRules() {
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("HelloWorld");
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setCount(2);
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
    }*/

}
