package com.nacos.orderserviceconsumer.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-09 14:37  星期四
 * @Description:
 */
@Data
public class Order {
    private int count;
    private BigDecimal money;
    private long productId;
    private long userId;
    private int status;

}
