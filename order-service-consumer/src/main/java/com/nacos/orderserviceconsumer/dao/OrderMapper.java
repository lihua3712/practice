package com.nacos.orderserviceconsumer.dao;

import com.nacos.orderserviceconsumer.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-10 14:08  星期五
 * @Description:
 */
@Repository
@Mapper
public interface OrderMapper {

    int insert(Order order);
}
