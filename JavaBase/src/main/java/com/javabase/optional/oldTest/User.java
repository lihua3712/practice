package com.javabase.optional.oldTest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-12 14:20  星期四
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String name;
    private String age;
    private String sex;
    private String email;
    private String xh;
}
