package com.javabase.optional.oldTest;

import java.util.Optional;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-03 14:35  星期五
 * @Description:
 */
public class Test {

    private static User user = null;

    static {
        user = new User();
        user.setName("xiaoming");
        user.setAge("24");
        user.setSex("男");
        user.setXh("66");
    }


    public static void main(String[] args) {
        String email = user.getEmail();
        String name = user.getName();
        System.out.println(email);
        System.out.println(name);

        Optional<User> user = Optional.ofNullable(Test.user);
        Optional<User> user1 = Optional.of(Test.user);
        Optional<String> optional = user.map(User::getName);
        /*User user2 = user1.orElse();
        User user3 = user1.orElseGet();
        User user4 = user1.orElseThrow();*/


        boolean present = optional.isPresent();
        System.out.println(present);

        optional.ifPresent(temp -> {
            System.out.println("用户temp不为空");
        });

        String s = optional.get();
        System.out.println(s);


    }

}
