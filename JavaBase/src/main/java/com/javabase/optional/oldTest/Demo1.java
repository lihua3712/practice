/*
package com.javabase.optional;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

*/
/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-12 14:16  星期四
 * @Description:java8新特性之Optional容器
 *//*

public class Demo1 {

    @Test(expected = NoSuchElementException.class)
    public void whenCreateEmptyOptional_thenNull() {
        Optional<User> emptyOpt = Optional.empty();
        emptyOpt.get();
    }

    @Test(expected = NullPointerException.class)
    public void whenCreateOfEmptyOptional_thenNullPointerException() {
        User user = new User();
        Optional<User> opt = Optional.of(user);
        User user1 = null;
        Optional<User> opt1 = Optional.ofNullable(user1);
    }

    @Test
    public void whenCreateOfNullableOptional_thenOk() {
        String name = null;
        Optional<String> opt = Optional.ofNullable(name);
        String actual = opt.get();
        System.out.println(actual);
    }


    @Test
    public void whenCheckIfPresent_thenOk() {
        User user = new User("john@gmail.com", "1234");
        Optional<User> opt = Optional.ofNullable(user);
        boolean present = opt.isPresent();
        assertTrue(present);

        assertEquals(user.getEmail(), opt.get().getEmail());
    }
}
*/
