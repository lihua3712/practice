package com.javabase.optional.newTest;

import java.util.Optional;

/**
 * @Author:lihua
 * @Date: 2023-02-17 15:17 星期五
 * @Version v1.0.0
 * @Description:
 */
public class OptionalTest {


    public static void main(String[] args) {
        // 声明一个空Optional
        Optional<Object> empty = Optional.empty();

        // 依据一个非空值创建Optional
        Student student = new Student();
        Optional<Student> os1 = Optional.of(student);

        // 可接受null的Optional
        Student student1 = null;
        Optional<Student> os2 = Optional.ofNullable(student1);

    }

}
