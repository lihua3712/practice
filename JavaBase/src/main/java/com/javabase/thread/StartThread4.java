package com.javabase.thread;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-02-28 17:22  星期一
 * @Description:模拟购票
 */
public class StartThread4 implements Runnable {

    private int ticketNums = 10;


    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                if (ticketNums <= 0) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "--->>拿到了第" + ticketNums-- + "票");
            }
        }
    }

    public static void main(String[] args) {
        StartThread4 startThread4 = new StartThread4();
        new Thread(startThread4, "小明").start();
        new Thread(startThread4, "小花").start();
        new Thread(startThread4, "小红").start();
    }
}
