package com.javabase.thread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-02-28 16:53  星期一
 * @Description: Thread Runnable Callable
 */
public class StartThread2 extends Thread {

    private String url;
    private String name;

    public StartThread2(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public void run() {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.downloader(url, name);
        System.out.println("下载了文件名为:" + name);
    }

    public static void main(String[] args) {
        StartThread2 t1 = new StartThread2("https://img.alicdn.com/imgextra/i4/3195154388/O1CN01Tnpbaq1iHkXfNYEE8_!!3195154388-0-daren.jpg_180x180xzq90.jpg_.webp", "1.jpg");
        StartThread2 t2 = new StartThread2("https://shcaishuitong.cdn.bcebos.com/newNotice/2021080578401628128217780.jpg", "2.jpg");
        StartThread2 t3 = new StartThread2("https://shcaishuitong.cdn.bcebos.com/newNotice/202108057291628130412724.jpg", "3.jpg");
        t1.start();
        t2.start();
        t3.start();
    }
}


class WebDownloader {
    public void downloader(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
    }
}
