package com.javabase.thread.testcallable;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.util.concurrent.*;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-03-01 14:39  星期二
 * @Description:Callable实现线程 好处：
 * 可以定义返回值
 * 可以抛出异常
 */
public class TestCallable implements Callable<Boolean> {

    private String url;
    private String name;

    public TestCallable(String url, String name) {
        this.url = url;
        this.name = name;
    }

    @Override
    public Boolean call() {
        WebDownloader webDownloader = new WebDownloader();
        webDownloader.downloader(url, name);
        System.out.println("下载了文件名为:" + name);
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        TestCallable t1 = new TestCallable("https://img.alicdn.com/imgextra/i4/3195154388/O1CN01Tnpbaq1iHkXfNYEE8_!!3195154388-0-daren.jpg_180x180xzq90.jpg_.webp", "1.jpg");
        TestCallable t2 = new TestCallable("https://shcaishuitong.cdn.bcebos.com/newNotice/2021080578401628128217780.jpg", "2.jpg");
        TestCallable t3 = new TestCallable("https://shcaishuitong.cdn.bcebos.com/newNotice/202108057291628130412724.jpg", "3.jpg");

        //创建执行服务
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        //提交执行
        Future<Boolean> submit1 = executorService.submit(t1);
        Future<Boolean> submit2 = executorService.submit(t2);
        Future<Boolean> submit3 = executorService.submit(t3);

        //获取结果
        Boolean rs1 = submit1.get();
        Boolean rs2 = submit2.get();
        Boolean rs3 = submit3.get();

        System.out.println(rs1);
        System.out.println(rs2);
        System.out.println(rs3);

        //关闭服务
        executorService.shutdown();
    }
}

class WebDownloader {
    public void downloader(String url, String name) {
        try {
            FileUtils.copyURLToFile(new URL(url), new File(name));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("IO异常");
        }
    }
}
