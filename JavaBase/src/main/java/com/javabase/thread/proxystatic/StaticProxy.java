package com.javabase.thread.proxystatic;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-03-01 14:59  星期二
 * @Description:
 */
public class StaticProxy {

    public static void main(String[] args) {
        You you = new You();

        new Thread(() -> System.out.println("我爱你")).start();


        WeddingCompany weddingCompany = new WeddingCompany(you);
        weddingCompany.HappyMarry();

    }
}

interface Marry {
    void HappyMarry();
}

class You implements Marry {
    @Override
    public void HappyMarry() {
        System.out.println("lihua will be marry he is very happy");
    }
}

class WeddingCompany implements Marry {
    private Marry target;

    public WeddingCompany(Marry target) {
        this.target = target;
    }

    @Override
    public void HappyMarry() {
        before();
        this.target.HappyMarry();
        after();
    }

    private void before() {
        System.out.println("结婚之前");
    }

    private void after() {
        System.out.println("结婚之后");
    }
}
