package com.javabase.thread;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-02-28 17:22  星期一
 * @Description:
 */
public class StartThread3 implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println("我在学习多线程---" + i);
        }
    }

    public static void main(String[] args) {
        StartThread3 startThread3 = new StartThread3();
        new Thread(startThread3).start();
        for (int i = 0; i < 200; i++) {
            System.out.println("我在看代码---" + i);
        }
    }
}
