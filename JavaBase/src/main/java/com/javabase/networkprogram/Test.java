package com.javabase.networkprogram;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-11-02 10:50  星期二
 * @Description:
 */
public class Test {

    public static void main(String[] args) {
        String str = null;
        String s1 = String.valueOf(str);
        String s2 = null + "";
        System.out.println(s1);
        System.out.println(s2);
    }
}
