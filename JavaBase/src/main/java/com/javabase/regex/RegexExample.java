package com.javabase.regex;

import cn.hutool.core.util.NumberUtil;

import java.util.regex.Pattern;

/**
 * 正则
 * ?和^?$ 区别
 */
public class RegexExample {
    public static void main(String[] args) {
        String regex1 = "[0-6]";
        String regex2 = "^[0-6]$";
        String input1 = "34";
        String input2 = "4";
        String input3 = "a";

        System.out.println("Using [0-6]:");
        System.out.println(input1 + ": " + Pattern.matches(regex1, input1)); // false
        System.out.println(input2 + ": " + Pattern.matches(regex1, input2)); // true
        System.out.println(input3 + ": " + Pattern.matches(regex1, input3)); // false

        System.out.println("\nUsing ^[0-6]$:");
        System.out.println(input1 + ": " + Pattern.matches(regex2, input1)); // false
        System.out.println(input2 + ": " + Pattern.matches(regex2, input2)); // true
        System.out.println(input3 + ": " + Pattern.matches(regex2, input3)); // false

        System.out.println("NumberUtil.isNumber = " + NumberUtil.isNumber("5%"));
        System.out.println("NumberUtil.isNumber = " + NumberUtil.isNumber("5"));
        System.out.println("NumberUtil.isValidNumber = " + NumberUtil.isValidNumber(null));
        System.out.println("NumberUtil.isValidNumber = " + NumberUtil.isValidNumber(null));
    }
}
