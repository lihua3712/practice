package com.javabase.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-03-22 17:18  星期二
 * @Description:
 */
public class Code {

    public static void main(String[] args) {
        /*for (int i = 0; i < 10; i++) {
            String random = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
            System.out.println(random);
        }*/


        /*for (int i = 0; i < 100; i++) {
            int flag = new Random().nextInt(1000000);
            String result = String.valueOf(flag);
            if (result.length() < 6) {
                result = String.format("%06d", flag);
            }
            System.out.println(result);
        }*/


        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        List<List<String>> lists = groupList(list);
        System.out.println("list:" + list.toString());
        System.out.println(lists);
    }


    /**
     * List分割
     *
     * @param list
     * @return
     */
    public static List<List<String>> groupList(List<String> list) {
        List<List<String>> listGroup = new ArrayList<List<String>>();
        int listSize = list.size();
        //子集合的长度
        int toIndex = 2;
        for (int i = 0; i < list.size(); i += 2) {
            if (i + 2 > listSize) {
                toIndex = listSize - i;
            }
            List<String> newList = list.subList(i, i + toIndex);
            listGroup.add(newList);
        }
        return listGroup;
    }
}
