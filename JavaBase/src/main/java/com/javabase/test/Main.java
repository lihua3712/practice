package com.javabase.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {
        // 创建 List<Object> 对象并初始化
        List<User> list = new ArrayList<>();
        list.add(new User("", "赵Alice123", 20));
        list.add(new User("", "钱Bob456", 21));
        list.add(new User("", "钱Bob4567", 22));
        list.add(new User("", "孙Charlie789", 22));
        list.add(new User("", "李David10", 23));
        list.add(new User("", "李David11", 24));

        // 定义正则表达式
        String pattern = "[a-zA-Z0-9]";

        // 使用 Stream API 进行处理
        Map<String, List<User>> result = list.stream()
                .peek(user -> {
                    // 对每个 User 对象进行处理，去除 name 中的数字和字母
                    String newName = user.getName().replaceAll(pattern, "");
                    user.setPrimaryName(newName);
                })
                .collect(Collectors.groupingBy(User::getPrimaryName));

        // 输出结果
        System.out.println(result);
        System.out.println(result.size());
    }
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class User {
    private String primaryName;
    private String name;
    private int age;
}
