package com.javabase.test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SundayListGenerator {

    public static List<String> generate(String startDateStr, String endDateStr) {
        List<String> sundayList = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate startDate = LocalDate.parse(startDateStr, formatter);
        LocalDate endDate = LocalDate.parse(endDateStr, formatter);
        LocalDate sunday = startDate.with(DayOfWeek.SUNDAY);
        while (!sunday.isAfter(endDate)) {
            sundayList.add(sunday.format(formatter));
            sunday = sunday.plusWeeks(1);
        }
        return sundayList;
    }

    public static void main(String[] args) {
        List<String> generate = generate("20230301", "20230331");
        generate.forEach(temp-> System.out.println(temp));
    }
}
