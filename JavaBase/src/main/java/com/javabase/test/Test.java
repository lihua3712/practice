package com.javabase.test;

import cn.hutool.core.date.DateUtil;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class Test {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.of(2022, 3, 1);
        LocalDate date2 = LocalDate.of(2022, 3, 15);

        // 计算两个日期之间的天数
        long daysBetween = ChronoUnit.DAYS.between(date1, date2);
        System.out.println("Days between: " + daysBetween);

        // 或者使用 Period 类计算天数
        Period period = Period.between(date1, date2);
        int days = period.getDays();
        System.out.println("Days between: " + days);





        String start = "2022-01-01";
        String end = "2022-01-10";

        long betweenDay = DateUtil.betweenDay(DateUtil.parse(end), DateUtil.parse(start), false);
        System.out.println(betweenDay);



    }
}
