package com.javabase.test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LIHUA
 */
public class SundayDatesInRange {

    public static void main(String[] args) {
        String startTime = "20230301";
        String endTime = "20231201";

        // 设置时间范围
        LocalDate startDate = LocalDate.parse(startTime, DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate endDate = LocalDate.parse(endTime, DateTimeFormatter.BASIC_ISO_DATE);
        // 获取时间范围内所有的周日日期
        List<String> sundayDates = new ArrayList<>();
        LocalDate date = startDate.with(TemporalAdjusters.lastDayOfMonth());
        if (!startDate.isEqual(date)) {
            sundayDates.add(startDate.format(DateTimeFormatter.BASIC_ISO_DATE));
        }
        while (!date.isAfter(endDate)) {
            sundayDates.add(date.format(DateTimeFormatter.BASIC_ISO_DATE));
            date = date.plusDays(1).with(TemporalAdjusters.lastDayOfMonth());
        }
        if (date.isAfter(endDate) && !sundayDates.contains(endTime)) {
            sundayDates.add(endDate.format(DateTimeFormatter.BASIC_ISO_DATE));
        }
        // 打印结果
        for (String sundayDate : sundayDates) {
            System.out.println(sundayDate);
        }
    }
}
