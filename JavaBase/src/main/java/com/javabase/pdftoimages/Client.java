package com.javabase.pdftoimages;


import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author LIHUA
 */
public class Client {

    /*public static void main(String[] args) throws Exception {
        testPdf2images();
    }*/

    /*private static void testPdf2images() throws Exception {
        File pdfFile = new File("F:/testpdf/阿里巴巴Java开发...1528284352.pdf");
        List<byte[]> images = pdf2images(pdfFile);
        AtomicInteger fileNameIndex = new AtomicInteger(1);
        for (byte[] image : images) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(image);
            FileOutputStream fileOutputStream = new FileOutputStream("F:/testpdf/image/" + fileNameIndex.getAndIncrement() + ".png");
            byteArrayInputStream.transferTo(fileOutputStream);
        }
    }*/

    /**
     * 将PDF文件转换成多张图片
     *
     * @param pdfFile PDF源文件
     * @return 图片字节数组列表
     */
    private static List<byte[]> pdf2images(File pdfFile) throws Exception {
        //加载PDF
        PDDocument pdDocument = PDDocument.load(pdfFile);
        //创建PDF渲染器
        PDFRenderer renderer = new PDFRenderer(pdDocument);
        int pages = pdDocument.getNumberOfPages();
        List<byte[]> images = new ArrayList<>();
        for (int i = 0; i < pages; i++) {
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            //将PDF的每一页渲染成一张图片
            BufferedImage image = renderer.renderImage(i);
            ImageIO.write(image, "png", output);
            images.add(output.toByteArray());
        }
        pdDocument.close();
        return images;
    }

}
