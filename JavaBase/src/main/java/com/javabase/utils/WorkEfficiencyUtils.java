package com.javabase.utils;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.Week;

import java.util.Date;

/**
 * 公司上班是时间为：周一到周五 上午9:00-12:30,下午14:00-18:00;一天共7.5h,
 * 业务员提一个工单，记创建时间为t1,
 * 对应售后人员去处理,记处理时间为t2,
 * 求处理时效（t2-t1）,单位为小时数。
 * 注：非工作时间,不计入时效(节假日不考虑,周六周日为非工作日)
 * 例如1: 业务员周一早上5点提的需求工单,售后人员上午十点处理，这个时效为10-9(上班时间)=1h
 * 例如2: 业务员上周六某时提的需求工单,售后人员下周一上午十点处理，这个时效为10-9(上班时间)=1h
 * 例如3: 业务员上周一下午15点提的需求工单,售后人员本周一上午十点处理，这个时效为 (18-15)+(7.5*4)+(10-9)=34h
 * 要求：
 * 1.用java实现
 * 2.代码逻辑清晰易懂
 * 3.计算性能较好
 * 举例：
 * 2023-07-03 10:00:00  到  2023-07-03 15:00:00 处理时效:  2.5+1=3.5小时
 * 2023-07-03 10:00:00  到  2023-07-04 15:00:00 处理时效:  6.5+4.5=11小时
 * 2023-07-03 10:00:00  到  2023-07-05 15:00:00 处理时效:  6.5+(7.5*1)+4.5=18.5小时
 * 2023-07-03 10:00:00  到  2023-07-10 15:00:00 处理时效:  6.5+(7.5*4)+4.5=41小时
 */
public class WorkEfficiencyUtils {

    // 上午的工作时间区间
    private static final String[] AM_DATE = {"09:00", "12:30"};

    // 下午的工作时间区间
    private static final String[] PM_DATE = {"14:00", "18:00"};

    // 上午工作区间完全覆盖的工作时长
    private static final float AM_TIME_UNIT = 3.5F;

    // 下午工作区间完全覆盖的工作时长
    private static final float PM_TIME_UNIT = 4.0F;

    private static final String HOUR_DATE_PATTERN = "HH:mm";


    // 时间状态的枚举
    private enum TimeState {
        BEFORE_WORK,     // 上班之前
        AM_WORKING,      // 上午上班时间
        IN_NOON_BREAK,   // 上午下班后，下午上班前
        PM_WORKING,      // 下午上班时间
        AFTER_WORK,      // 下班之后
    }

    public static float getWorkEfficiency(Date startDate, Date endDate, boolean ignoreWeekend) {
        // 不过滤周末
        if (!ignoreWeekend) {
            return getWorkEfficiency(startDate, endDate);
        }

        Date adjustStartDate = correctionDate(startDate);

        Date adjustEndDate = correctionDate(endDate);

        float totalHour = getWorkEfficiency(adjustStartDate, adjustEndDate);

        int days = getTotalWeekendDays(adjustStartDate, adjustEndDate);

        return totalHour - days * (AM_TIME_UNIT + PM_TIME_UNIT);
    }

    private static int getTotalWeekendDays(Date startDate, Date endDate) {
        Date tempDate = DateUtil.endOfWeek(startDate);

        int sumDays = 0;

        while (tempDate.compareTo(endDate) < 0) {
            sumDays += 2;
            tempDate = DateUtil.offsetDay(tempDate, 7);
        }

        return sumDays;
    }

    private static Date correctionDate(Date date) {
        if (DateUtil.dayOfWeekEnum(date).equals(Week.SATURDAY) || DateUtil.dayOfWeekEnum(date).equals(Week.SUNDAY)) {
            return DateUtil.offsetDay(DateUtil.beginOfWeek(DateUtil.calendar(date)).getTime(), 7);
        }
        return date;
    }


    private static float doSameWorkingCompute(long day, Date startDate, Date endDate, float unitTime, String[] compute) {
        if (day >= 1) {
            float startDifference = getTimeDifference(compute[1], startDate);
            float endDifference = getTimeDifference(compute[0], endDate);
            return endDifference - startDifference + unitTime + (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT);
        } else {
            long between = DateUtil.between(startDate, endDate, DateUnit.MINUTE);
            return (((float) (between / 60L)) + ((between % 60L) / 60.0F));
        }
    }

    public static float getWorkEfficiency(Date startDate, Date endDate) {
        if (startDate.compareTo(endDate) >= 0) {
            return 0.0F;
        }

        // 获取开始时间落地的位置
        TimeState startTimeState = getTimeState(startDate);

        // 获取结束时间落地的位置
        TimeState endTimeState = getTimeState(endDate);

        // 判断是否夸天的标识，大于零跨天了，等于零表示没有跨天
        long day = DateUtil.betweenDay(startDate, endDate, true);

        // 开始日期和结束日期落地的区间相同
        if (startTimeState.equals(endTimeState)) {

            if (TimeState.BEFORE_WORK.equals(startTimeState) ||
                    TimeState.IN_NOON_BREAK.equals(startTimeState) ||
                    TimeState.AFTER_WORK.equals(startTimeState)) {
                // 落地在非工作区间是，可以直接数出他们的工作区间
                return day * (AM_TIME_UNIT + PM_TIME_UNIT);
            } else {
                // 当落地在工作区间时
                if (TimeState.AM_WORKING.equals(startTimeState)) {
                    return doSameWorkingCompute(day, startDate, endDate, PM_TIME_UNIT, AM_DATE);
                }
                if (TimeState.PM_WORKING.equals(startTimeState)) {
                    return doSameWorkingCompute(day, startDate, endDate, AM_TIME_UNIT, PM_DATE);
                }
            }
        }


        // 开始日期落地的区间小于结束日期落地的区间
        if (startTimeState.ordinal() < endTimeState.ordinal()) {
            if (endTimeState.ordinal() - startTimeState.ordinal() == 1) {

                if (TimeState.BEFORE_WORK.equals(startTimeState)) {
                    float difference = getTimeDifference(AM_DATE[0], endDate);
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.AM_WORKING.equals(startTimeState)) {
                    float difference = getTimeDifference(AM_DATE[1], startDate);

                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) - difference;
                }

                if (TimeState.IN_NOON_BREAK.equals(startTimeState)) {
                    float difference = getTimeDifference(PM_DATE[0], endDate);
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.PM_WORKING.equals(startTimeState)) {
                    float difference = getTimeDifference(PM_DATE[1], startDate);
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) - difference;
                }

            }

            if (endTimeState.ordinal() - startTimeState.ordinal() == 2) {

                if (TimeState.BEFORE_WORK.equals(startTimeState)) {
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + AM_TIME_UNIT;
                }

                if (TimeState.AM_WORKING.equals(startTimeState)) {
                    float difference = getTimeDifference(PM_DATE[0], endDate) - getTimeDifference(AM_DATE[1], startDate);

                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.IN_NOON_BREAK.equals(startTimeState)) {
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + PM_TIME_UNIT;
                }
            }

            if (endTimeState.ordinal() - startTimeState.ordinal() == 3) {

                if (TimeState.BEFORE_WORK.equals(startTimeState)) {
                    float difference = getTimeDifference(PM_DATE[0], endDate) + AM_TIME_UNIT;

                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.AM_WORKING.equals(startTimeState)) {
                    float difference = PM_TIME_UNIT - getTimeDifference(AM_DATE[1], startDate);

                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

            }

            if (endTimeState.ordinal() - startTimeState.ordinal() == 4) {
                return (day + 1) * (AM_TIME_UNIT + PM_TIME_UNIT);
            }
        }

        // 开始日期落地的区间大于结束日期落地的区间
        if (startTimeState.ordinal() > endTimeState.ordinal()) {
            if (startTimeState.ordinal() - endTimeState.ordinal() == 1) {

                if (TimeState.BEFORE_WORK.equals(endTimeState)) {
                    float difference = getTimeDifference(AM_DATE[0], startDate);
                    return day * (AM_TIME_UNIT + PM_TIME_UNIT) - difference;
                }

                if (TimeState.AM_WORKING.equals(endTimeState)) {
                    float difference = getTimeDifference(AM_DATE[0], endDate) + PM_TIME_UNIT;

                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.IN_NOON_BREAK.equals(endTimeState)) {
                    float difference = AM_TIME_UNIT - getTimeDifference(PM_DATE[1], startDate);
                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.PM_WORKING.equals(endTimeState)) {
                    float difference = AM_TIME_UNIT + getTimeDifference(PM_DATE[0], endDate);
                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

            }

            if (startTimeState.ordinal() - endTimeState.ordinal() == 2) {

                if (TimeState.BEFORE_WORK.equals(endTimeState)) {
                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + PM_TIME_UNIT;
                }

                if (TimeState.AM_WORKING.equals(endTimeState)) {
                    float difference = getTimeDifference(AM_DATE[0], endDate) - getTimeDifference(PM_DATE[1], startDate);

                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

                if (TimeState.IN_NOON_BREAK.equals(endTimeState)) {
                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + AM_TIME_UNIT;
                }
            }

            if (startTimeState.ordinal() - endTimeState.ordinal() == 3) {

                if (TimeState.BEFORE_WORK.equals(endTimeState)) {
                    float difference = getTimeDifference(PM_DATE[1], startDate);

                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) - difference;
                }

                if (TimeState.AM_WORKING.equals(endTimeState)) {
                    float difference = getTimeDifference(AM_DATE[0], endDate);

                    return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT) + difference;
                }

            }

            if (startTimeState.ordinal() - endTimeState.ordinal() == 4) {
                return (day - 1) * (AM_TIME_UNIT + PM_TIME_UNIT);
            }
        }

        return 0.0F;
    }

    // 获取日期落地在那个时间区间内了
    private static TimeState getTimeState(Date date) {
        String dateTime = DateUtil.format(date, HOUR_DATE_PATTERN);

        if (dateTime.compareTo(AM_DATE[0]) < 0) {
            return TimeState.BEFORE_WORK;
        }

        if (dateTime.compareTo(AM_DATE[0]) >= 0 && dateTime.compareTo(AM_DATE[1]) <= 0) {
            return TimeState.AM_WORKING;
        }

        if (dateTime.compareTo(PM_DATE[0]) >= 0 && dateTime.compareTo(PM_DATE[1]) <= 0) {
            return TimeState.PM_WORKING;
        }

        if (dateTime.compareTo(PM_DATE[1]) > 0) {
            return TimeState.AFTER_WORK;
        }

        return TimeState.IN_NOON_BREAK;
    }

    // 计算某个日期，与时间区间的某个值相比的小时差
    private static float getTimeDifference(String startTimeStr, Date date) {
        String tempEndDateStr = DateUtil.format(date, DatePattern.NORM_DATE_PATTERN);
        Date startEndDate = DateUtil.parse(new StringBuilder(tempEndDateStr).append(" ").append(startTimeStr));
        long between = DateUtil.between(startEndDate, date, DateUnit.MINUTE);

        if (startEndDate.compareTo(date) >= 0) {
            return -(((float) (between / 60L)) + ((between % 60L) / 60.0F));
        }

        return (((float) (between / 60L)) + ((between % 60L) / 60.0F));
    }


    public static void main(String[] args) {
        float workEfficiency = getWorkEfficiency(DateUtil.parse("2023-07-05 09:22:00"),
                DateUtil.parse("2023-07-05 10:22:00"), false);

        Date date = DateUtil.beginOfWeek(DateUtil.calendar(DateUtil.parse("2023-07-04 07:30:00")))
                .getTime();

        System.out.println(workEfficiency);
    }


}
