package com.javabase.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class WeekMainTemp {
    public static void main(String[] args) {
        // 假设这是一个Integer类型的日期有序集合
        List<Integer> dateList = Arrays.asList(20211001, 20220302, 20211230, 20211112, 20220305, 20220306,
                20220307, 20220308, 20220309, 20220310, 20211230, 20221230, 20220313, 20220314, 20220315, 20220323);

        // 将Integer类型的日期转换为LocalDate类型
        List<LocalDate> localDateList = dateList.stream()
                .map(date -> LocalDate.parse(date.toString(), DateTimeFormatter.BASIC_ISO_DATE))
                .sorted()
                .collect(Collectors.toList());

        // 按照年和周进行分组
        Map<Integer, LocalDate> yearWeekLastDateMap = localDateList.stream()
                .collect(Collectors.groupingBy(date -> date.get(WeekFields.ISO.weekOfYear()),
                                Collectors.collectingAndThen(
                                        Collectors.reducing((a, b) -> b), Optional::get)
                        )
                );

        // 按照日期排序并转换为Integer类型
        List<Integer> result = yearWeekLastDateMap.values().stream()
                .sorted()
                .map(date -> Integer.parseInt(date.format(DateTimeFormatter.BASIC_ISO_DATE)))
                .collect(Collectors.toList());

        // 输出结果
        System.out.println(result);
    }


}
