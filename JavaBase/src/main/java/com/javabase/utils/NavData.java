package com.javabase.utils;

import lombok.Data;

/**
 * @Author:lihua
 * @Date: 2023-04-12 13:27 星期三
 * @Version v1.0.0
 * @Description:
 */
@Data
public class NavData {

    private Integer date;//日期
    private double value;//净值
}
