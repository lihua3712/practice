package com.javabase.sortAlgorithm;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

/**
 * @Author:lihua
 * @Date: 2023-02-23 14:41 星期四
 * @Version v1.0.0
 * @Description:
 */
public class SortUtil {

    @Getter
    @Setter
    @AllArgsConstructor
    static class SortTest {
        private String name;
        private int a;
        private int b;
        private int c;
        private Map<Integer, String> d;
    }

    /*public static void main(String[] args) {
        List<Integer> flag = Arrays.asList(1, 2, 3);
        // 测试List多属性排序
        List<SortTest> list = new ArrayList<>();
        Map<Integer, String> d1 = new HashMap<>();
        d1.put(1, "1");
        d1.put(2, "2");
        Map<Integer, String> d2 = new HashMap<>();
        d2.put(1, "3");
        d2.put(3, "4");
        Map<Integer, String> d3 = new HashMap<>();
        d3.put(2, "3");
        d3.put(3, "2");
        list.add(new SortTest("S1", 7, 3, 7, d1));
        list.add(new SortTest("S2", 9, 5, 5, d1));
        list.add(new SortTest("S3", 5, 7, 4, d1));
        list.add(new SortTest("S4", 1, 5, 4, d3));
        list.add(new SortTest("S5", 4, 5, 4, d2));
        list.add(new SortTest("S6", 5, 6, 1, d1));
        list.add(new SortTest("S7", 5, 7, 1, d3));
        list.add(new SortTest("S8", 5, 3, 7, d1));
        list.add(new SortTest("S9", 5, 7, 7, d2));

        // 先按a倒序排列、再按b倒序排列、最后按c正序排列
        *//*Comparator<SortTest> c = Comparator.comparing(SortTest::getA, Comparator.reverseOrder())
                .thenComparing(SortTest::getB, Comparator.reverseOrder())
                .thenComparing(SortTest::getC);
        list.sort(c);*//*
        System.out.println("==============================start=============================================");
        for (SortTest sortTest : list) {
            System.out.println(sortTest.getName() + "\t" + sortTest.getA() + "\t" + sortTest.getB() + "\t" + sortTest.getC());
        }
        List<SortTest> newList = CollUtil.sort(list, invoke(flag));
        System.out.println("===============================end============================================");
        for (SortTest sortTest : newList) {
            System.out.println(sortTest.getName() + "\t" + sortTest.getA() + "\t" + sortTest.getB() + "\t" + sortTest.getC());
        }

    }*/

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    static class TestDto {
        private String name;
        private Map<Integer, String> map;
    }

    public static void main(String[] args) {
        List<TestDto> list = new ArrayList<>();
        TestDto testDto1 = new TestDto();
        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1, "1");
        map1.put(4, "4");
        testDto1.setName("S1");
        testDto1.setMap(map1);
        list.add(testDto1);
        TestDto testDto2 = new TestDto();
        Map<Integer, String> map2 = new HashMap<>();
        map2.put(6, "6");
        map2.put(2, "2");
        testDto2.setName("S2");
        testDto2.setMap(map2);
        list.add(testDto2);
        TestDto testDto3 = new TestDto();
        Map<Integer, String> map3 = new HashMap<>();
        map3.put(5, "5");
        map3.put(2, "2");
        testDto3.setName("S3");
        testDto3.setMap(map3);
        list.add(testDto3);
        TestDto testDto4 = new TestDto();
        Map<Integer, String> map4 = new HashMap<>();
        map4.put(1, "1");
        map4.put(2, "2");
        testDto4.setName("S4");
        testDto4.setMap(map4);
        list.add(testDto4);

        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        ids.add(4);
        System.out.println("===============================start============================================");
        for (TestDto testDto : list) {
            System.out.println(testDto.getName() + "\t");
        }
        list.sort(invoke1(ids));
        System.out.println("===============================end============================================");
        for (TestDto testDto : list) {
            System.out.println(testDto.getName() + "\t");
        }

    }


    public static Comparator<TestDto> invoke(List<Integer> flag) {
        Integer integer = flag.get(0);
        Comparator<TestDto> result = new Comparator<TestDto>() {
            @Override
            public int compare(TestDto o1, TestDto o2) {
                String s1 = StrUtil.isNotBlank(o1.getMap().get(integer)) ? o1.getMap().get(integer) : "";
                String s2 = StrUtil.isNotBlank(o2.getMap().get(integer)) ? o2.getMap().get(integer) : "";
                if (s1.compareTo(s2) > 0) {
                    return 1;
                } else if (s1.compareTo(s2) < 0) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
        for (int i = 1; i < flag.size(); i++) {
            result = result.thenComparing(temp -> StrUtil.isNotBlank(temp.getMap().get(integer)) ? temp.getMap().get(integer) : "");
        }
        return result;
    }

    public static Comparator<TestDto> invoke1(List<Integer> flag) {
        Integer integer = flag.get(0);
        Comparator<TestDto> result = Comparator.comparing(temp -> StrUtil.isNotBlank(temp.getMap().get(integer)) ? temp.getMap().get(integer) : "");
        for (int i = 1; i < flag.size(); i++) {
            result = result.thenComparing(temp -> StrUtil.isNotBlank(temp.getMap().get(integer)) ? temp.getMap().get(integer) : "");
        }
        return result;
    }

}
