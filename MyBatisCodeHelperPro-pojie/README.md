# IDEA全版本MyBatisCodeHelper Pro免费使用教程（全部版本适用）

## 版本说明:
1. 系统：macOS（Windows同理）
2. IDE：IntelliJ IDEA 2023.1.3 （哪个版本都可以）
3. MyBatisCodeHelper Pro 3.2.1（哪个版本都可以）

## 环境准备
1. 从插件市场下载MyBatisCodeHelper Pro
![img.png](image/img.png)
2. 领取临时激活码
 
   领取地址：https://brucege.com/pay/getfreetrial
 ![img_1.png](image/img_1.png)
 ![img_2.png](image/img_2.png)
3. 使插件处于试用状态

   输入刚刚申请的试用码
![img_3.png](image/img_3.png)
试用成功：
![img_4.png](image/img_4.png)
4. 下载cfr的jar包

   下载地址：https://github.com/leibnitz27/cfr/releases

   下最新的就行：
![img_5.png](image/img_5.png)

## 正式教程
**找到MyBatisCodeHelper Pro的jar包**
1. Windows：在地址栏输入%appdata%\JetBrains\找到idea安装目录，里面有个plugins文件夹，然后里面有个文件夹MyBatisCodeHelper-Pro
2. MacOS：/Users/用户名/Library/Application Support/JetBrains/IntelliJIdea2023.1/plugins/MyBatisCodeHelper-Pro

**MacOS的有一点特殊，注意用户名目录和你的idea版本号**

然后进入到lib文件夹，找到**MyBatisCodeHelper-Pro-obfuss.jar。**
![img_6.png](image/img_6.png)

将它和刚刚下载的cfr的jar包拷贝到另一个地方，比如桌面，方便修改。
![img_7.png](image/img_7.png)

## 反编译jar包

1. 在刚刚两个jar包的目录打开终端（Windows是打开cmd，一个意思）。
![img_8.png](image/img_8.png)

2. 执行命令： 

`java -jar cfr-0.152.jar MyBatisCodeHelper-Pro-obfuss.jar --renamedupmembers true --hideutf false >> result.txt
`

需要注意cfr版本号，如果你跟我下载的不是一个版本，需要更改为指定版本。

过程会比较慢，耐心等待…

执行完毕文件夹中会出现一个result的文本文件：
![img_9.png](image/img_9.png)
![img_10.png](image/img_10.png)

## 修改前准备/介绍/目标

**准备/介绍**

用一个比较方便的工具打开上面的result.txt。

* 搜索validTo，找到具有paidKey,valid,validTo的类

  **不同插件版本，这个类的位置会不一样**
 ![img_11.png](image/img_11.png)

* valid为判断是否在有效期的变量，变量定义为下方的private Boolean d

* validTo为有效时长变量，定义为下方的private Long e;

* package com.ccnode.codegenerator.T.e是当前类所在的包，类名为c，由此可知当前类的绝对定位为com.ccnode.codegenerator.T.e.b
![img_12.png](image/img_12.png)

**目标（这是我们将要用代码实现的目标，先演练）**
1. 修改变量d，也就是代表是否有效的那个变量的get/set方法始终为true

**(再次提醒：不同的插件版本变量名称也不一样，你要看清楚！)**

```
// ------------get方法-----------
public Boolean b() {
	return this.d;
}

// 改为：

public Boolean b() {
	return true;
}

// ------------set方法-----------
public void a(Boolean bl) {
	this.d = bl;
}

// 改为：

public void a(Boolean bl) {
	this.d = true;
}

```
![img_13.png](image/img_13.png)
2. 修改变量e，也就是那个代表有效期还剩多久的变量，get/set方法为固定值

**(再次提醒：不同的插件版本变量名称也不一样，你要看清楚！)**

```
// ------------get方法-----------
public Long e() {
return this.e;
}

// 改为：

public Long e() {
return new Long(4797976044000L);
}

// ------------set方法-----------
public void a(Long l2) {
this.e = l2;
}

// 改为：

public void a(Long l2) {
this.e = new Long(4797976044000L);
}
```
![img_14.png](image/img_14.png)

## 代码实现

1. 使用IDEA创建一个Maven项目
![img_15.png](image/img_15.png)
 
2. 导入Javassist依赖
```
<!-- javassist -->
<dependency>
	<groupId>org.javassist</groupId>
	<artifactId>javassist</artifactId>
	<version>3.28.0-GA</version>
</dependency>
```
3. 代码实现

以下代码需要注意几点：

* pool.insertClassPath("MyBatisCodeHelper-Pro-obfuss.jar"); 需要填写MyBatisCodeHelper-Pro-obfuss.jar在你本机实际的路径！
* cc.writeFile("/Users/localhost/Desktop/test"); 需要填写你能找到的输出路径
* 还有就是代码中体现的包名，类名，变量名，都要和上面咱们准备的时候一直，这个一定要注意！！！
* 代码都有注释！注意看！

```
package org.example;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

/**
 * Hello world!
 * @author localhost
 */
public class App {
    public static void main(String[] args) {
        try {
            ClassPool pool = ClassPool.getDefault();
            // 此处改为你实际的的 MyBatisCodeHelper-Pro-obfuss.jar 的路径
            pool.insertClassPath("/Users/localhost/Library/Application Support/JetBrains/IntelliJIdea2023.1/plugins/MyBatisCodeHelper-Pro/lib/MyBatisCodeHelper-Pro-obfuss.jar");
            // 通过绝对定位，加载指定的类
            CtClass cc = pool.get("com.ccnode.codegenerator.T.e.b");

            // 获取validTo的get方法
            // public Long e()
            CtMethod getValidToMethod = cc.getDeclaredMethod("e");

            // 获取validTo的set方法的参数：Long
            CtClass[] params = new CtClass[] { pool.get("java.lang.Long") };
            // 获取validTo的set方法
            // public void a(Long l2)
            CtMethod setValidToMethod = cc.getDeclaredMethod("a", params);

            // 获取valid的set方法的参数：Boolean
            CtClass[] params1 = new CtClass[] { pool.get("java.lang.Boolean") };
            // 获取Valid的set方法
            // public void a(Boolean bl)
            CtMethod setValidMethod = cc.getDeclaredMethod("a", params1);

            // 获取valid的get方法
            // public Boolean b()
            CtMethod getValidMethod = cc.getDeclaredMethod("b");

            // 修改validTo的get方法
            // 直接返回4797976044000
            StringBuilder builder = new StringBuilder();
            builder.append("{")
                    .append("       return new Long(4797976044000L);")
                    .append("}");
            getValidToMethod.setBody(builder.toString());

            // 修改validTo的set方法
            // 直接设为4797976044000
            StringBuilder builder1 = new StringBuilder();
            builder1.append("{")
                    .append("        this.e = new Long(4797976044000L);")
                    .append("}");
            setValidToMethod.setBody(builder1.toString());

            // 修改valid的set方法
            // 设为True
            String getValidMethodBuilder = "{" +
                    "       return Boolean.TRUE;" +
                    "}";
            getValidMethod.setBody(getValidMethodBuilder);

            // 修改valid的get方法
            // 直接返回True
            String setValidMethodBuilder = "{" +
                    "this.d = Boolean.TRUE;" +
                    "}";
            setValidMethod.setBody(setValidMethodBuilder);

            // 将修改后的Class b写入指定文件夹
            cc.writeFile("/Users/localhost/Desktop/test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```
运行上面的代码后会在你指定的文件夹中生成修改的Class b：
![img_16.png](image/img_16.png)
打开看一下是不是修改成功了：
![img_17.png](image/img_17.png)

### 替换Class文件

1. 用压缩工具打开刚刚拷贝出来的MyBatisCodeHelper-Pro-obfuss.jar
![img_18.png](image/img_18.png)
2. 找到com.ccnode.codegenerator.T.e.b并替换成我们刚刚修改的Class文件

   直接拖进去：
![img_19.png](image/img_19.png)
   然后关闭压缩工具即可。
3. 替换jar包

将我们拷贝出来并修改完的MyBatisCodeHelper-Pro-obfuss.jar，替换我们开头找到的原始MyBatisCodeHelper-Pro-obfuss.jar。

**为防止出问题，注意先备份再替换！！！**
![img_20.png](image/img_20.png)

## 重启,查看是否成功
![img_21.png](image/img_21.png)