package com.mq.rabbitmq;

import com.mq.rabbitmq.service.producer.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RabbitMqApplicationTests {

    @Autowired
    OrderService orderService;

    @Test
    public void contextLoads() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Thread.sleep(2000);
            Long userId = 100L + i;
            orderService.makeOrderFanout(String.valueOf(userId));
        }
    }

    @Test
    public void contextLoadsDirect() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Thread.sleep(3000);
            Long userId = 100L + i;
            orderService.makeOrderDirect(String.valueOf(userId));
        }
    }

    @Test
    public void contextLoadsTopic() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Thread.sleep(2000);
            Long userId = 100L + i;
            orderService.makeOrderTopic(String.valueOf(userId));
        }
    }

    @Test
    public void contextLoadsTTL() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Thread.sleep(100);
            Long userId = 100L + i;
            orderService.makeOrderTTL(String.valueOf(userId));
        }
    }

    @Test
    public void contextLoadsTTLMessage() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Thread.sleep(100);
            Long userId = 100L + i;
            orderService.makeOrderTTLMessage(String.valueOf(userId));
        }
    }

}
