package com.mq.rabbitmq.java.topics;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @author: 学相伴-飞哥
 * @description: Producer 简单队列生产者
 * @Date : 2021/3/2
 */
public class Producer {
    public static void main(String[] args) throws IOException {
        //通过工具类获取连接对象
        Connection connection = RabbitMQUtils.getConnection("topic生产者");
        //获取连接中通道
        Channel channel = connection.createChannel();
        //交换机
        String exchangeName = "topic-exchange";
        //声明交换机
        channel.exchangeDeclare(exchangeName, "topic", true);

        /**
         * # 统配符
         * 		* (star) can substitute for exactly one word.    匹配不多不少恰好1个词
         * 		# (hash) can substitute for zero or more words.  匹配0个或多个词
         * # 如:
         * 		audit.#    匹配audit.irs.corporate或者 audit.irs 等
         *     audit.*   只能匹配 audit.irs
         */
        //都可以收到 queue-1 queue-2
        String routingKey1 = "com.course.order";
        //都可以收到 queue-1 queue-3
        String routingKey2 = "com.order.user";
        String routingKey3 = "com.#";

        //绑定交换机和队列的关系
        channel.queueBind("queue1", exchangeName, routingKey1);
        channel.queueBind("queue2", exchangeName, routingKey2);
        channel.queueBind("queue3", exchangeName, routingKey3);
        /**
         * 发送消息给中间件rabbitmq-server
         * @params1: 交换机exchange
         * @params2: 队列名称/routingkey
         * @params3: 属性配置
         * @params4: 发送消息的内容
         */
        //准备发送消息的内容
        channel.basicPublish(exchangeName, routingKey1, null, ("指定的route key " + routingKey1 + "的消息").getBytes());
        channel.basicPublish(exchangeName, routingKey2, null, ("指定的route key " + routingKey2 + "的消息").getBytes());
        channel.basicPublish(exchangeName, routingKey3, null, ("指定的route key " + routingKey3 + "的消息").getBytes());
        System.out.println("消息发送成功!");
        RabbitMQUtils.closeConnectionAndChanel(channel, connection);
    }
}
