package com.mq.rabbitmq.java.simple;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-12-30 9:58  星期四
 * @Description:
 */
public class Consumer {

    public static void main(String[] args) throws IOException {

        //通过工具类获取连接
        Connection connection = RabbitMQUtils.getConnection("消费者");

        //创建通道
        Channel channel = connection.createChannel();

        /**
         * 消费消息
         * 参数1: 消费那个队列的消息 队列名称
         * 参数2: 开始消息的自动确认机制,自动应答/手动应答
         * 参数3: 消费时的回调接口
         */
        channel.basicConsume("lihua",
                true,
                (consumerTag, message) -> System.out.println("收到的消息是：" + new String(message.getBody(), "UTF-8")),
                s -> System.out.println("接收失败了"));
    }
}
