package com.mq.rabbitmq.java.simple;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @author: 学相伴-飞哥
 * @description: Producer 简单队列生产者
 * @Date : 2021/3/2
 */
public class Producer {
    public static void main(String[] args) throws IOException {
        //通过工具类获取连接对象
        Connection connection = RabbitMQUtils.getConnection("生产者");

        //获取连接中通道
        Channel channel = connection.createChannel();

        /**
         * 申明队列queue存储消息
         * 如果队列不存在，则会创建
         * Rabbitmq不允许创建两个相同的队列名称，否则会报错。
         * @params1： queue 队列的名称
         * @params2： durable 队列是否持久化
         * @params3： exclusive 是否排他，即是否私有的，如果为true,会对当前队列加锁，其他的通道不能访问，并且连接自动关闭
         * @params4： autoDelete 是否自动删除，当最后一个消费者断开连接之后是否自动删除消息。
         * @params5： arguments 可以设置队列附加参数，设置队列的有效期，消息的最大长度，队列的消息生命周期等等。
         */
        //队列名称
        String queueName = "lihua";
        channel.queueDeclare(queueName, true, false, false, null);
        /**
         * 发送消息给中间件rabbitmq-server
         * @params1: 交换机exchange
         * @params2: 队列名称/routing
         * @params3: 属性配置
         * @params4: 发送消息的内容
         */
        //准备发送消息的内容
        String message = "你好，学相伴！！！";
        channel.basicPublish("", queueName, null, message.getBytes());
        System.out.println("消息发送成功!");

        //调用工具类
        RabbitMQUtils.closeConnectionAndChanel(channel, connection);
    }
}
