package com.mq.rabbitmq.java.fanout;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @author: 学相伴-飞哥
 * @description: Producer 简单队列生产者
 * @Date : 2021/3/2
 */
public class Producer {
    public static void main(String[] args) throws IOException {
        //通过工具类获取连接对象
        Connection connection = RabbitMQUtils.getConnection("fanout生产者");
        //获取连接中通道
        Channel channel = connection.createChannel();

        //交换机
        String exchangeName = "fanout_exchange";

        //声明交换机,@Param3持久化
        channel.exchangeDeclare(exchangeName, "fanout", true);
        /**
         * 发送消息给中间件rabbitmq-server
         * @params1: 交换机exchange
         * @params2: 队列名称/routingkey
         * @params3: 属性配置
         * @params4: 发送消息的内容
         */
        //routingKey
        String routingKey = "";
        //准备发送消息的内容
        String message = "你好，学相伴！！！";
        channel.basicPublish(exchangeName, routingKey, null, message.getBytes());

        System.out.println("消息发送成功!");
        RabbitMQUtils.closeConnectionAndChanel(channel, connection);
    }
}
