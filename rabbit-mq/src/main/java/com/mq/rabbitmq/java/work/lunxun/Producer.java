package com.mq.rabbitmq.java.work.lunxun;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

/**
 * @author: 学相伴-飞哥
 * @description: Producer 简单队列生产者
 * @Date : 2021/3/2
 */
public class Producer {
    public static void main(String[] args) throws IOException {
        //通过工具类获取连接对象
        Connection connection = RabbitMQUtils.getConnection("work-lunxun生产者");
        //获取连接中通道
        Channel channel = connection.createChannel();
        // 6： 准备发送消息的内容
        //===============================end topic模式==================================
        for (int i = 1; i <= 20; i++) {
            //消息的内容
            String msg = "学相伴:" + i;
            /**
             * 发送消息给中间件rabbitmq-server
             * @params1: 交换机exchange
             * @params2: 队列名称/routingkey
             * @params3: 属性配置
             * @params4: 发送消息的内容
             */
            channel.basicPublish("", "queue1", null, msg.getBytes());
        }
        System.out.println("消息发送成功!");
        RabbitMQUtils.closeConnectionAndChanel(channel, connection);
    }
}
