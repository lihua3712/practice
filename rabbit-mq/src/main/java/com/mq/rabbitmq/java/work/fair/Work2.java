package com.mq.rabbitmq.java.work.fair;

import com.mq.rabbitmq.java.utils.RabbitMQUtils;
import com.rabbitmq.client.*;

/**
 * @author: 学相伴-飞哥
 * @description: Consumer
 * @Date : 2021/3/2
 */
public class Work2 {
    public static void main(String[] args) {
        Connection connection = null;
        Channel channel = null;
        try {
            connection = RabbitMQUtils.getConnection("fair-work2消费者");
            channel = connection.createChannel();
            // 5: 申明队列queue存储消息
            /**
             *  如果队列不存在，则会创建
             *  Rabbitmq不允许创建两个相同的队列名称，否则会报错。
             *  @params1： queue 队列的名称
             *  @params2： durable 队列是否持久化
             *  @params3： exclusive 是否排他，即是否私有的，如果为true,会对当前队列加锁，其他的通道不能访问，并且连接自动关闭
             *  @params4： autoDelete 是否自动删除，当最后一个消费者断开连接之后是否自动删除消息。
             *  @params5： arguments 可以设置队列附加参数，设置队列的有效期，消息的最大长度，队列的消息生命周期等等。
             */
            // 这里如果queue已经被创建过一次了，可以不需要定义
            //channel.queueDeclare("queue1", false, true, false, null);
            // 同一时刻，服务器只会推送一条消息给消费者
            //channel.basicQos(1);
            // 6： 定义接受消息的回调
            Channel finalChannel = channel;
            finalChannel.basicQos(1);
            finalChannel.basicConsume("queue1",
                    false,
                    (s, delivery) -> {
                        try {
                            System.out.println("Work2-收到消息是：" + new String(delivery.getBody(), "UTF-8"));
                            Thread.sleep(200);
                            finalChannel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    },
                    s -> System.out.println("接收失败了"));
            System.out.println("Work2-开始接受消息");
            System.in.read();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("发送消息出现异常...");
        } finally {
            RabbitMQUtils.closeConnectionAndChanel(channel, connection);
        }
    }
}
