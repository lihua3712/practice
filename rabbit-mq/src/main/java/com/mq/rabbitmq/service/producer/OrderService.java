package com.mq.rabbitmq.service.producer;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author: 学相伴-飞哥
 * @description: OrderService
 * @Date : 2021/3/4
 */
@Component
public class OrderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void makeOrderFanout(String userId) {
        String orderNumer = UUID.randomUUID().toString();
        System.out.println("用户 " + userId + ",订单编号是：" + orderNumer);
        // 发送订单信息给RabbitMQ fanout
        // 1: 定义交换机
        String exchangeName = "fanout_order_exchange";
        // 2: 路由key
        String routeKey = "";
        rabbitTemplate.convertAndSend(exchangeName, routeKey, orderNumer);
    }

    public void makeOrderDirect(String userId) {
        String orderNumer = UUID.randomUUID().toString();
        System.out.println("用户 " + userId + ",订单编号是：" + orderNumer);
        // 发送订单信息给RabbitMQ direct
        // 1: 定义交换机
        String exchangeName = "direct_order_exchange";
        // 2: 路由key
        rabbitTemplate.convertAndSend(exchangeName, "sms", orderNumer);
        rabbitTemplate.convertAndSend(exchangeName, "weixin", orderNumer);
    }

    public void makeOrderTopic(String userId) {
        String orderNumer = UUID.randomUUID().toString();
        System.out.println("用户 " + userId + ",订单编号是：" + orderNumer);
        // 发送订单信息给RabbitMQ direct
        // 1: 定义交换机
        String exchangeName = "topic_order_exchange";
        // 2: 路由key
        /**
         * #.email.#
         * *.sms.#
         * weixin.#
         */
        String routeKey = "weixin.email";
        rabbitTemplate.convertAndSend(exchangeName, routeKey, orderNumer);
    }

    /**
     * 设置队列消息过期时间
     *
     * @param userId
     */
    public void makeOrderTTL(String userId) {
        String orderNumer = UUID.randomUUID().toString();
        System.out.println("用户 " + userId + ",订单编号是：" + orderNumer);
        // 发送订单信息给RabbitMQ direct
        // 1: 定义交换机
        String exchangeName = "ttl_direct_exchange";
        // 2: 路由key
        String routeKey = "ttl";
        rabbitTemplate.convertAndSend(exchangeName, routeKey, orderNumer);
    }

    /**
     * 设置队列消息过期时间
     *
     * @param userId
     */
    public void makeOrderTTLMessage(String userId) {
        String orderNumer = UUID.randomUUID().toString();
        System.out.println("用户 " + userId + ",订单编号是：" + orderNumer);
        // 发送订单信息给RabbitMQ direct
        // 1: 定义交换机
        String exchangeName = "ttl_direct_exchange";
        // 2: 路由key
        String routeKey = "ttlMessage";

        //给消息设置过期时间
        MessagePostProcessor messagePostProcessor = message -> {
            message.getMessageProperties().setExpiration("5000");
            message.getMessageProperties().setContentEncoding("UTF-8");
            return message;
        };
        rabbitTemplate.convertAndSend(exchangeName, routeKey, orderNumer, messagePostProcessor);
    }

}
