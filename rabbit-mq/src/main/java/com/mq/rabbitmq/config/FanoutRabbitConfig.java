package com.mq.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author : JCccc
 * @CreateTime : 2019/9/3
 * @Description :fanout交换机
 **/
@Configuration
public class FanoutRabbitConfig {

    @Bean
    public FanoutExchange fanoutOrderExchange() {
        return new FanoutExchange("fanout_order_exchange", true, false);
    }

    @Bean
    public Queue smsFanoutQueue() {
        return new Queue("sms.fanout.queue", true);
    }

    @Bean
    public Queue emailFanoutQueue() {
        return new Queue("email.fanout.queue", true);
    }

    @Bean
    public Queue weixinFanoutQueue() {
        return new Queue("weixin.fanout.queue", true);
    }

    @Bean
    public Binding bindingFanout1() {
        return BindingBuilder.bind(smsFanoutQueue()).to(fanoutOrderExchange());
    }

    @Bean
    public Binding bindingFanout2() {
        return BindingBuilder.bind(emailFanoutQueue()).to(fanoutOrderExchange());
    }

    @Bean
    public Binding bindingFanout3() {
        return BindingBuilder.bind(weixinFanoutQueue()).to(fanoutOrderExchange());
    }
}
