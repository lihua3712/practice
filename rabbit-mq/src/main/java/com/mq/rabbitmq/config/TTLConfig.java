package com.mq.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author : JCccc
 * @CreateTime : 2019/9/3
 * @Description :ttl设置消息队列过期时间
 **/
@Configuration
public class TTLConfig {

    @Bean
    public DirectExchange ttlDirectExchange() {
        return new DirectExchange("ttl_direct_exchange", true, false);
    }

    @Bean
    public Queue ttlDirectQueue() {
        Map<String, Object> args = new HashMap<>();
        //设置队列过期时间
        args.put("x-message-ttl", 5000);
        //设置队列消息最大数目
        args.put("x-max-length", 5);
        //设置死信交换机
        args.put("x-dead-letter-exchange", "dead_direct_exchange");
        //设置路由key,fanout模式不用配置
        args.put("x-dead-letter-routing-key", "dead");
        return new Queue("ttl.direct.queue", true, false, false, args);
    }

    @Bean
    public Queue ttlDirectMessageQueue() {
        return new Queue("ttl.direct.message.queue", true);
    }

    @Bean
    public Binding bindingTTL() {
        return BindingBuilder.bind(ttlDirectQueue()).to(ttlDirectExchange()).with("ttl");
    }

    @Bean
    public Binding bindingTTLMessage() {
        return BindingBuilder.bind(ttlDirectMessageQueue()).to(ttlDirectExchange()).with("ttlMessage");
    }
}
