package com.mq.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author : JCccc
 * @CreateTime : 2019/9/3
 * @Description :死信队列配置
 **/
@Configuration
public class DeadConfig {

    @Bean
    public DirectExchange deadDirectExchange() {
        return new DirectExchange("dead_direct_exchange", true, false);
    }

    @Bean
    public Queue deadDirectQueue() {
        return new Queue("dead.direct.queue", true);
    }


    @Bean
    public Binding bindingDead() {
        return BindingBuilder.bind(deadDirectQueue()).to(deadDirectExchange()).with("dead");
    }
}
