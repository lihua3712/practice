package com.mq.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author LIHUA
 * 交换机队列定义在消费者更好
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class RabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqApplication.class, args);
    }


    /**
     * 设置内存大小
     * 命令：rabbitmqctl set_vm_memory_high_watermark relative 0.4
     * 命令：rabbitmqctl set_vm_memory_high_watermark absolute 50MB
     * 设置磁盘大小
     * 命令：rabbitmqctl set_disk_free_limit  100G
     *
     * 内存换页
     * vm_memory_high_watermark.relative = 0.4
     * vm_memory_high_watermark_paging_ratio = 0.7(设置值小于1)
     *
     */
}
