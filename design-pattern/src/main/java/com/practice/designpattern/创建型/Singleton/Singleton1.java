package com.practice.designpattern.创建型.Singleton;

/**
 * 饿汉式单例写法
 * 饿汉式单例在类加载初始化时就创建好一个静态的对象供外部使用，
 * 除非系统重启，这个对象不会改变，所以本身就是线程安全的。
 * 缺点：java反射机制支持访问private属性，所以可通过反射来破解该构造方法，
 * 产生多个实例
 */
public class Singleton1 {

    private Singleton1() {
    }

    private static Singleton1 singleton1 = new Singleton1();

    //静态工厂方法
    public static Singleton1 getInstance() {
        return singleton1;
    }
}
