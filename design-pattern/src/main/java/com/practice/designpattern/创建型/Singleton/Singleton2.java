package com.practice.designpattern.创建型.Singleton;

/**
 * 懒汉式写法
 * 双重检查，指的是两次检查 instance 是否为 null。
 * volatile 在这里是需要的，希望能引起读者的关注。
 * 很多人不知道怎么写，直接就在 getInstance() 方法签名上加上 synchronized，这就不多说了，性能太差。
 */
public class Singleton2 {

    private Singleton2() {
    }

    private static volatile Singleton2 single = null;


    //静态工厂方法
    public static Singleton2 getInstance() {
        synchronized (Singleton2.class) {
            if (single == null) {
                single = new Singleton2();
            }
        }
        return single;
    }
}
