package com.practice.designpattern.创建型.simpleFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-18 15:31  星期三
 * @Description:
 */
public class HuangMenChicken extends Food {
    public HuangMenChicken() {
        System.out.println("我在吃HuangMenChicken。。。");
    }

    @Override
    public void addSpicy(String more) {
        super.addSpicy(more);
        System.out.println("我是子类HuangMenChicken加辣。。。");
    }

    @Override
    public void addCondiment(String potato) {
        super.addCondiment(potato);
        System.out.println("我是子类HuangMenChicken添加调味品。。。");
    }
}
