package com.practice.designpattern.创建型.abstractFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 10:32  星期四
 * @Description:
 */
public class Computer {

    private CPU cpu;
    private MainBoard board;
    private HardDisk hardDisk;

    public Computer() {
    }

    public Computer(CPU cpu, MainBoard board, HardDisk hardDisk) {
        this.cpu = cpu;
        this.board = board;
        this.hardDisk = hardDisk;
    }
}
