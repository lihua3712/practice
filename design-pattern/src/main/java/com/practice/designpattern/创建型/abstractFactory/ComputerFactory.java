package com.practice.designpattern.创建型.abstractFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 10:30  星期四
 * @Description:抽象工厂
 */
public interface ComputerFactory {

    /**
     * 制作cpu
     *
     * @param name
     * @return
     */
    CPU makeCPU(String name);

    /**
     * 制作主板
     *
     * @param name
     * @return
     */
    MainBoard makeMainBoard(String name);

    /**
     * 制作硬盘
     *
     * @param name
     * @return
     */
    HardDisk makeHardDisk(String name);
}
