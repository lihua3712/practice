package com.practice.designpattern.创建型.simpleFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-18 15:30  星期三
 * @Description:
 */
public class Food {

    public Food() {
        System.out.println("我是一个食物。。。");
    }

    public void addSpicy(String more) {
        System.out.println("我是父类加辣。。。");
    }

    public void addCondiment(String potato) {
        System.out.println("我是父类添加调味品。。。");
    }
}
