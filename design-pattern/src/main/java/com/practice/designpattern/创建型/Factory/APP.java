package com.practice.designpattern.创建型.Factory;

/**
 * 工厂模式
 * 往往需要使用两个或两个以上的工厂。
 */
public class APP {
    public static void main(String[] args) {
        // 先选择一个具体的工厂
        FoodFactory factory = new ChineseFoodFactory();
        // 由第一步的工厂产生具体的对象，不同的工厂造出不一样的对象
        Food food = factory.makeFood("A");
    }
}
