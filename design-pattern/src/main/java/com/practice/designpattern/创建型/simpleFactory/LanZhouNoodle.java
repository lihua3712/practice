package com.practice.designpattern.创建型.simpleFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-18 15:30  星期三
 * @Description:
 */
public class LanZhouNoodle extends Food {
    public LanZhouNoodle() {
        System.out.println("我在吃LanZhouNoodle。。。");
    }

    @Override
    public void addSpicy(String more) {
        System.out.println("我是子类LanZhouNoodle加辣。。。");
        //super.addSpicy(more);
    }

    @Override
    public void addCondiment(String potato) {
        super.addCondiment(potato);
        System.out.println("我是子类LanZhouNoodle添加调味品。。。");
    }
}
