package com.practice.designpattern.创建型.Singleton;

public class Person {
    private Person() {
        //此类外部无法使用new关键字进行实例化
    }

    public static Person getInstance() {
        return new Person();
    }
}
