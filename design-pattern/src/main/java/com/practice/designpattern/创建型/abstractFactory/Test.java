package com.practice.designpattern.创建型.abstractFactory;

import com.practice.designpattern.创建型.Factory.ChineseFoodFactory;
import com.practice.designpattern.创建型.Factory.Food;
import com.practice.designpattern.创建型.Factory.FoodFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 10:40  星期四
 * @Description:抽象工厂 抽象工厂的问题也是显而易见的，比如我们要加个显示器，就需要修改所有的工厂，
 * 给所有的工厂都加上制造显示器的方法。这有点违反了对修改关闭，对扩展开放这个设计原则。
 */
public class Test {
    public static void main(String[] args) {
        // 先选择一个具体的工厂
        ComputerFactory intelFactory = new IntelFactory();
        // 由第一步的工厂产生具体的对象，不同的工厂造出不一样的对象
        CPU a = intelFactory.makeCPU("A");
        MainBoard a1 = intelFactory.makeMainBoard("A");
        HardDisk a2 = intelFactory.makeHardDisk("A");
        // 将同一个厂子出来的 CPU、主板、硬盘组装在一起
        Computer result = new Computer(a, a1, a2);
        System.out.println(result.toString());
    }
}
