package com.practice.designpattern.创建型.Factory;


public interface FoodFactory {
    Food makeFood(String name);
}


