package com.practice.designpattern.创建型.Singleton;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021/7/23 13:55  星期五
 * @Description:单例模式
 */
public class SingletonMode {
    /**
     * 说明：一个类仅有一个实例，由自己创建并对外提供一个实例获取的入口，
     * 外部类可以通过这个入口直接获取该实例对象。
     * 场景：
     */
    public static void main(String[] args) {
        Person instance = Person.getInstance();
    }
}








