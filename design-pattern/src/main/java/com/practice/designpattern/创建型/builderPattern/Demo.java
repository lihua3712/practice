package com.practice.designpattern.创建型.builderPattern;

import lombok.Builder;
import lombok.ToString;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 15:06  星期四
 * @Description:
 */
@Builder
@ToString
class Demo {
    private String name;
    private String password;
    private String nickName;
    private int age;
}
