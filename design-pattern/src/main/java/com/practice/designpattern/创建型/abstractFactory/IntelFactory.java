package com.practice.designpattern.创建型.abstractFactory;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 10:54  星期四
 * @Description:
 */
public class IntelFactory implements ComputerFactory {
    @Override
    public CPU makeCPU(String name) {
        System.out.println("IntelFactory生产cpu");
        return null;
    }

    @Override
    public MainBoard makeMainBoard(String name) {
        System.out.println("IntelFactory生产mainboard");
        return null;
    }

    @Override
    public HardDisk makeHardDisk(String name) {
        System.out.println("IntelFactory生产harddisk");
        return null;
    }

    public MainBoard aa(String name) {
        return null;
    }
}
