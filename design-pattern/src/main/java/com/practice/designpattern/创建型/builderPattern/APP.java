package com.practice.designpattern.创建型.builderPattern;

/**
 * 建造者模式
 */
public class APP {
    public static void main(String[] args) {
        User build = User.builder()
                .name("foo")
                .password("pAss12345")
                .nickName("hjuahua")
                .age(25)
                .build();
        System.out.println(build.toString());

        Demo demo = Demo.builder()
                .name("lihua")
                .password("123")
                .nickName("hjuahua")
                .age(24)
                .build();
        System.out.println(demo.toString());
    }
}
