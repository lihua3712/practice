package com.practice.designpattern.结构性.bridgeMode;

public interface DrawAPI {
    public void draw(int radius, int x, int y);
}
