package com.practice.designpattern.结构性.proxyPattern;

public interface FoodService {

    Food makeChicken();

    Food makeNoodle();
}
