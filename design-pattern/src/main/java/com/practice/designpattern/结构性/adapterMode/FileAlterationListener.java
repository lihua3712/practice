package com.practice.designpattern.结构性.adapterMode;

import java.io.File;

public interface FileAlterationListener {
    //void onStart(final FileAlterationObserver observer);

    void onDirectoryCreate(final File directory);

    void onDirectoryChange(final File directory);

    void onDirectoryDelete(final File directory);

    void onFileCreate(final File file);

    void onFileChange(final File file);

    void onFileDelete(final File file);

    //void onStop(final FileAlterationObserver observer);
}
