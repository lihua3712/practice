package com.practice.designpattern.结构性.bridgeMode;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-20 11:21  星期五
 * @Description:桥梁模式
 */
public class Test {
    public static void main(String[] args) {
        Shape greenCircle = new Circle(10, new GreenPen());
        Shape redRectangle = new Rectangle(4, 8, new RedPen());

        greenCircle.draw();
        redRectangle.draw();
    }

}
