package com.practice.designpattern.结构性.proxyPattern;

import lombok.Data;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-08-19 16:41  星期四
 * @Description:
 */
@Data
public class Chicken extends Food {
    private String chicken;
}
