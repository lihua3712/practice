package com.example.elasticsearch.utils;

import com.example.elasticsearch.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-02-11 14:31  星期五
 * @Description:
 */
@Component
public class HtmlParseUtil {

    /**
     * 获取请求jd http://search.jd.com/Search?keyword=java
     * 前提需要联网！ Ajax数据不能获取到
     * https://www.qcc.com/web/search?key=%E8%B4%A2%E7%A8%8E%E9%80%9A%E8%BD%AF%E4%BB%B6%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8
     */
    public List<Content> parseJD(String keyword) throws IOException {
        String url = "http://search.jd.com/Search?keyword=" + keyword;
        Document document = Jsoup.parse(new URL(url), 30000);
        Element element = document.getElementById("J_goodsList");
        Elements elements = element.getElementsByTag("li");
        ArrayList<Content> arrayList = new ArrayList<>();
        for (Element el : elements) {
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
            Content content = new Content();
            content.setImg(img);
            content.setPrice(price);
            content.setTitle(title);
            arrayList.add(content);
        }
        return arrayList;
    }



}
