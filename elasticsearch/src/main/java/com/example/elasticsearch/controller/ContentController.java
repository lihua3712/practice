package com.example.elasticsearch.controller;

import com.example.elasticsearch.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2022, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2022-02-11 15:15  星期五
 * @Description:
 */
@RestController
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("/parseContent/{keyword}")
    public Boolean parseContent(@PathVariable("keyword") String keyword) throws IOException {
        Boolean aBoolean = contentService.parseContent(keyword);
        return aBoolean;
    }


    @GetMapping("/searchContent/{keyword}/{pageNo}/{pageSize}")
    public List<Map<String, Object>> searchContent(@PathVariable("keyword") String keyword,
                                                   @PathVariable("pageNo") int pageNo,
                                                   @PathVariable("pageSize") int pageSize) throws IOException {
        //List<Map<String, Object>> maps = contentService.searchContent(keyword, pageNo, pageSize);
        List<Map<String, Object>> maps = contentService.searchAndHighLighter(keyword, pageNo, pageSize);
        return maps;
    }


}
