package com.sangeng.handler;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class SGSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        System.out.println("认证成功了");

        /**
         *         //获得用户名
         *         User user = (User) authentication.getPrincipal();
         *         //将用户名生成jwt token
         *         String token = JwtUtil.generateToken(user.getUsername(), RsaUtil.privateKey, JwtUtil.EXPIRE_MINUTES);
         *         //将token 发送给前端
         *         UserVO userVo = new UserVO(user.getUsername(),token);
         *         ResponseResult.write(response,ResponseResult.ok(userVo));
         *         log.info("user:{}  token:{}",user.getUsername() , token);
         */
    }
}
