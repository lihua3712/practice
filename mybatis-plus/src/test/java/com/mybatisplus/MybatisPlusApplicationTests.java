package com.mybatisplus;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mybatisplus.mapper.UserMapper;
import com.mybatisplus.entity.User;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;


    @Test
    public void contest() {
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }

    /**
     * 查询操作
     */
    @Test
    public void select() {
        System.err.println("-------------1-----------------");

        List<User> users1 = userMapper.selectList(null);
        users1.forEach(System.out::println);

        System.err.println("-------------2-----------------");

        // 1、查询用户信息
        User user2 = userMapper.selectById(1L);
        System.out.println(user2);

        System.err.println("--------------3----------------");

        List<User> users3 = userMapper.selectBatchIds(Arrays.asList(1, 2, 3));
        users3.forEach(System.out::println);

        System.err.println("--------------4----------------");

        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("name", "Macrohua");
        objectObjectHashMap.put("age", 3);
        List<User> users4 = userMapper.selectByMap(objectObjectHashMap);
        users4.forEach(System.out::println);

        System.err.println("--------------5----------------");
    }


    /**
     * 测试分页查询
     */
    @Test
    public void testPage() {
        //参数一：当前页
        //参数二：页面大小
        //使用了分页插件之后，所有的分页操作也变得简单的！
        Page<User> page = new Page<>(2, 2);
        IPage<User> userIPage = userMapper.selectPage(page, null);
        System.out.println("userIPage.getTotal() = " +userIPage.getTotal());
        System.out.println("userIPage.getCurrent() = " +userIPage.getCurrent());
        System.out.println("userIPage.getSize() = " +userIPage.getSize());
        System.out.println("userIPage.getPages() = " +userIPage.getPages());
        userIPage.getRecords().forEach(System.out::println);
    }


    @Test
    public void insert() {
        User user = new User();
        user.setName("Macrohua");
        user.setAge(99);
        user.setEmail("Macrohua");
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }

    @Test
    public void update() {
        User user = new User();
        user.setName("lihua");
        user.setAge(18);
        user.setId(14L);
        int insert = userMapper.updateById(user);
        System.out.println(insert);
    }

    /**
     * 物理删除
     */
    @Test
    public void delete() {
        int insert = userMapper.deleteById(13L);
        System.out.println(insert);

        System.err.println("--------------1---------------");

        int i = userMapper.deleteBatchIds(Arrays.asList(1, 2, 3));
        System.out.println(i);

        System.err.println("---------------2--------------");


        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("name", "lihua");
        int i1 = userMapper.deleteByMap(stringObjectHashMap);
        System.out.println(i1);

        System.err.println("---------------3--------------");

    }

    /**
     * 逻辑删除
     */
    @Test
    public void logicDelete() {
        int insert = userMapper.deleteById(4L);
        System.out.println(insert);

        int batchIds = userMapper.deleteBatchIds(Arrays.asList(1, 2, 3));
        System.out.println(batchIds);
    }


    /**
     * 测试乐观锁成功！
     */
    @Test
    public void testOptimisticLocker() {
        // 1、查询用户信息
        User user = userMapper.selectById(1L);
        // 2、修改用户信息
        user.setName("kuangshen");
        user.setEmail("24736743@qq.com");
        // 3、执行更新操作
        userMapper.updateById(user);
    }

    /**
     * 测试乐观锁失败！多线程下
     */
    @Test
    public void testOptimisticLocker2() {
        //线程1
        User user = userMapper.selectById(1L);
        user.setName("lihua1111");
        user.setEmail("111111@qq.com");

        //模拟另一个线程执行了插入操作
        User user1 = userMapper.selectById(1L);
        user1.setName("lihua22222");
        user1.setEmail("22222@qq.com");
        userMapper.updateById(user1);

        //自旋锁进行多次尝试操作
        userMapper.updateById(user);
    }

}
