package com.mybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-10-20 13:41  星期三
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    /**
     * AUTO               主键自增
     * NONE              无主键
     * INPUT            自己输入
     * ID_WORKER       雪花主键
     * UUID           UUID
     * ID_WORKER_STR 字符串雪花主键
     */
    @TableId(type = IdType.ID_WORKER)
    private Long id;

    private String name;
    private Integer age;
    private String email;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 乐观锁Version注解
     */
    @Version
    private Integer version;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted;
}
