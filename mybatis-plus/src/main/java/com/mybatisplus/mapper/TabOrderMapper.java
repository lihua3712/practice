package com.mybatisplus.mapper;

import com.mybatisplus.entity.TabOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lihua
 * @since 2022-05-25
 */
@Mapper
public interface TabOrderMapper extends BaseMapper<TabOrder> {

}
