package com.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-10-20 13:43  星期三
 * @Description:在对应的Mapper上集成基础BaseMapper
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    //所有的CRUD已经写完,不需要配置其他


}
