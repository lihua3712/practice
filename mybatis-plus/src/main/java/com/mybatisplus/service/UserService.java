package com.mybatisplus.service;

import com.mybatisplus.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lihua
 * @since 2022-05-25
 */
public interface UserService extends IService<User> {

}
