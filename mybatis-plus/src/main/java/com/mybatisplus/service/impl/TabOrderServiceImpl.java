package com.mybatisplus.service.impl;

import com.mybatisplus.entity.TabOrder;
import com.mybatisplus.mapper.TabOrderMapper;
import com.mybatisplus.service.TabOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lihua
 * @since 2022-05-25
 */
@Service
public class TabOrderServiceImpl extends ServiceImpl<TabOrderMapper, TabOrder> implements TabOrderService {

}
