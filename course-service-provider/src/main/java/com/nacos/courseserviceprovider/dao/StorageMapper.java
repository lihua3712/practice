package com.nacos.courseserviceprovider.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-10 15:47  星期五
 * @Description:
 */
@Repository
public interface StorageMapper {

    int updateStorage(long productId);
}
