package com.nacos.courseserviceprovider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIHUA
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.nacos.courseserviceprovider.dao")
public class CourseServiceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseServiceProviderApplication.class, args);
    }


    /**
     * 新增restTemplate对象注入方法，注意，此处LoadBalanced注解一定要加上，否则无法远程调用
     *
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
