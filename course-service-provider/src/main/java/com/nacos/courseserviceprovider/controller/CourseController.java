package com.nacos.courseserviceprovider.controller;

import com.nacos.courseserviceprovider.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author LIHUA
 */
@Slf4j
@RestController
@RequestMapping("course")
@RefreshScope //动态刷新配置，重要
public class CourseController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("list")
    public String get() {
        return "已经访问到课程服务";
    }

    @GlobalTransactional
    @GetMapping("seatatest")
    public String seatatest(long userId, long productId) {
        log.info("减少库存开始。。。");
        Boolean aBoolean = storageService.updateStorage(productId);
        log.info("减少库存结束。。。");
        if (aBoolean) {
            log.info("开始下单。。。");
            String result = restTemplate.getForObject("http://order-service/order/create?userId=" + userId + "&productId=" + productId + "", String.class);
            log.info("下单结束。。。");
            return "下单结束：" + result;
        } else {
            return "库存更新失败";
        }
    }
}
