package com.nacos.courseserviceprovider.service.impl;

import com.nacos.courseserviceprovider.dao.StorageMapper;
import com.nacos.courseserviceprovider.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-10 15:37  星期五
 * @Description:
 */
@Slf4j
@Service
public class StorageServiceImpl implements StorageService {

    @Autowired
    private StorageMapper storageMapper;

    @Override
    public Boolean updateStorage(long productId) {
        log.info("更新库存开始");
        int index = storageMapper.updateStorage(productId);
        log.info("更新库存结束");
        return index > 0;
    }
}
