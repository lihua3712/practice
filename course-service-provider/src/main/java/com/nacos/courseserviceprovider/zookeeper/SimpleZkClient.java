package com.nacos.courseserviceprovider.zookeeper;

import org.apache.zookeeper.*;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author LIHUA
 * java代码操作zookeeper测试
 */
public class SimpleZkClient {

    /**
     * zookeeper集群服务器ip端口
     */
    private static final String CONNECT_STRING = "192.168.226.126:2181,192.168.226.126:2181,192.168.226.126:2181";
    private static final int SESSION_TIMEOUT = 2000;
    /**
     * 用来测试创建 /eclipse 节点
     */
    private static final String PATH = "/eclipse";
    public ZooKeeper zkClient = null;

    /**
     * 初始化 Zookeeper 对象
     *
     * @throws Exception
     */
    @Before
    public void init() throws Exception {
        // 第三个参数为回调函数
        zkClient = new ZooKeeper(CONNECT_STRING, SESSION_TIMEOUT, new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                // 收到事件通知后的回调函数
                System.out.println(event.getType() + "---" + event.getPath());
            }
        });
    }

    /**
     * 创建 znode
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    @Test
    public void testCreate() throws KeeperException, InterruptedException {
        // 参数1: 要创建的节点的路径 参数2: 节点数据  参数3: 节点权限  参数4:节点的类型
        zkClient.create(PATH, "zk-test".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        zkClient.create(PATH + "/node1", "zk-node1".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        zkClient.create(PATH + "/node2", "zk-node2".getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    /**
     * 获取PATH下的子znode列表
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    @Test
    public void testGetChildren() throws KeeperException, InterruptedException {
        List<String> children = zkClient.getChildren(PATH, true);
        for (String child : children) {
            System.out.println(child);
        }
    }

    /**
     * 获取PATH的znode的数据data
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    @Test
    public void testGetData() throws KeeperException, InterruptedException {
        byte[] data = zkClient.getData(PATH, true, null);
        System.out.println(new String(data));
    }

    /**
     * 判断节点路径是否存在
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    @Test
    public void testExist() throws KeeperException, InterruptedException {
        // 存在则返回Stat 元数据对象, 不存在返回null
        Stat exists = zkClient.exists(PATH, false);
        System.out.println(exists);
        System.out.println(exists == null ? "not exist" : "exist");
    }

    /**
     * 删除叶子节点， 若路径参数不为叶子节点，删除会报错.
     * Zookeeper对象 不支持删除非叶子节点，只能删除完子节点，才能删除父节点
     *
     * @throws InterruptedException
     * @throws KeeperException
     */
    @Test
    public void testDelete() throws InterruptedException, KeeperException {
        // 参数1： 删除的节点路径  参数2： 指定删除的版本， -1表示删除所有版本
        zkClient.delete(PATH + "/node1", -1);
    }


    /**
     * 修改路径节点数据
     *
     * @throws Exception
     * @throws InterruptedException
     */
    @Test
    public void testSetZnode() throws Exception, InterruptedException {
        zkClient.setData(PATH, "modify".getBytes(), -1);
        byte[] data = zkClient.getData(PATH, false, null);
        System.out.println(new String(data));
    }
}
