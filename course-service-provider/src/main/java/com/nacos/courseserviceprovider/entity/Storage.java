package com.nacos.courseserviceprovider.entity;

import lombok.Data;

/**
 * @version v1.0.0
 * @Copyright (C), 2016-2021, 财税通软件有限公司
 * @Author: lihua
 * @Date: 2021-09-10 15:37  星期五
 * @Description:
 */
@Data
public class Storage {
}
